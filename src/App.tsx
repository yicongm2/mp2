import React, {useEffect, useRef, useState} from 'react';
import './App.css';
import {
    BrowserRouter as Router,
    Route,
    Routes,
    Link
} from "react-router-dom";

import GalleryView, {Pokemon} from './components/Gallery'
import SearchView from './components/Search'
import PokemonDetails from "./components/Details";
import axios from "axios";
import DetailList from "./components/DetailList";





function App() {
    const [pokemonList, setPokemonList] = useState<Pokemon[]>([]);
    const isMounted = useRef(true);

    const api = axios.create({
        baseURL: 'https://pokeapi.co/api/v2/'
    })
    // const types = new Set<string>();
    useEffect(() => {
        // console.log('app start')
        const fetchData = async () => {
            const tempPokemons: Pokemon[] = [];
            for (let i = 1; i <= 1000; i++) {
                try {
                    const resp = await api.get('pokemon/' + i)
                    const res = resp.data;
                    const typesTemp : string[] = []
                    for(let j = 0; j < res.types.length; j++){
                        // types.add(res.types[j].type.name)
                        typesTemp.push(res.types[j].type.name)
                    }

                    const temp = {
                        idx: i - 1,
                        name: res.name,
                        img: res.sprites.front_default,
                        height: res.height,
                        weight: res.weight,
                        typeList: typesTemp,
                    };
                    tempPokemons.push(temp);
                } catch (error) {
                    console.error('Error fetching data: ', error);
                }
            }
            // setPokemonList(tempPokemons);
            if (isMounted.current) {
                setPokemonList(tempPokemons);
            }
        };
        fetchData();

        return () => {
            isMounted.current = false;
        };


    });

    // console.log(pokemonList)
  return (
      <Router>
    <div className="App">
      <header className="App-header">
          <Link to='/gallery' className="custom-link">Gallery</Link>
          <label className="icon">Pokemon Database</label>
          <Link to='/search' className="custom-link">Search</Link>
      </header>
        <div className='dividing'></div>
        <Routes>
            <Route path='/gallery' element={<GalleryView pokemonData={pokemonList} />}/>
            <Route path='/search' element={<SearchView pokemonData={pokemonList} />}/>
            <Route path='/' element={<DetailList pokemonData={pokemonList} />} />
            <Route path="/pokemon/:id" element={<PokemonDetails pokemonData={pokemonList} />} />
        </Routes>
    </div>
      </Router>
  );
}

export default App;

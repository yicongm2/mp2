import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import "./main.css";

export interface Pokemon {
    idx: number;
    name: string;
    img: string;
    height: number;
    weight: number;
    typeList: string[];
}

const typeArr = [
    "grass",
    "poison",
    "fire",
    "flying",
    "water",
    "bug",
    "normal",
    "electric",
    "ground",
    "fairy",
    "fighting",
    "psychic",
    "rock",
    "steel",
    "ice",
    "ghost",
    "dragon",
    "dark",
];

const GalleryView = (props: any) => {
    const [pokemonList, setPokemonList] = useState<Pokemon[]>([]);
    const [filteredPokemon, setfilteredPokemon] = useState<Pokemon[]>([]);
    const [selectedFilters, setSelectedFilters] = useState<string[]>([]);
    const [buttonStates, setButtonStates] = useState<boolean[]>(Array(typeArr.length).fill(false));


    useEffect(() => {
        setPokemonList(props.pokemonData);
        setfilteredPokemon(props.pokemonData);
    }, [props.pokemonData]);

    const setFilter = (types: string, index: number) => {
        const updatedFilters = [...selectedFilters];
        const updatedButtonStates = [...buttonStates];
        const currentIndex = updatedFilters.indexOf(types);

        if (currentIndex !== -1) {
            updatedFilters.splice(currentIndex, 1);
            updatedButtonStates[index] = false;
        } else {
            updatedFilters.push(types);
            updatedButtonStates[index] = true;
        }

        setSelectedFilters(updatedFilters);
        setButtonStates(updatedButtonStates);

        if (updatedFilters.length === 0) {
            setfilteredPokemon(pokemonList);
        } else {
            const filteredPokemons = pokemonList.filter((pokemon) =>
                updatedFilters.every((filter) => pokemon.typeList.includes(filter))
            );
            setfilteredPokemon(filteredPokemons);
        }
    };

    return (
        <div className="gallery">
            <div className="gallery-filter">
                {typeArr.map((item, index) => (
                    <button
                        className={`btn ${buttonStates[index] ? "active" : ""}`}
                        onClick={() => setFilter(item, index)}
                        key={item}
                    >
                        {item}
                    </button>
                ))}
            </div>

            <div className="pokemon-gallery">
                {filteredPokemon.map((pokemon: Pokemon, index: number) => (
                    <div className="pokemon-show" key={index}>
                        <Link to={`/pokemon/${pokemon.idx}`} className="pokemon-link">
                            <img
                                src={pokemon.img}
                                className="pokemon-img"
                                alt={pokemon.name}
                            />
                        </Link>
                        <p className="pokemon-name">
                            {pokemon.idx + " " + pokemon.name[0].toUpperCase() + pokemon.name.slice(1)}
                        </p>
                    </div>
                ))}
            </div>
        </div>
    );
};

export default GalleryView;

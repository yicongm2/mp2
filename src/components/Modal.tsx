import React, {Component, useEffect, useState} from 'react';
import {Pokemon} from './Gallery'
import Modal from "react-modal";

const ModalView = (props: any) => {

    const [queriedPokemonList, setQueriedPokemonList] = useState<Pokemon[]>([]);
    const [selectedPokemon, setSelectedPokemon] = useState<Pokemon | null>(null);
    const [selectedPokemonIndex, setSelectedPokemonIndex] = useState<number | null>(null);

    useEffect(() => {
            // setPokemonList(props.pokemonData)
            // setQueriedPokemonList(props.pokemonData)
        if(props.selectedPkm){
            setSelectedPokemon(props.selectedPkm);
            setQueriedPokemonList(props.queriedPkm);
            setSelectedPokemonIndex(props.selectedPkmIdx);
        }


        // console.log(queriedPokemonList)

    }, [props.selectedPkm]);


    const closeModal = () => {
        setSelectedPokemon(null);
        setSelectedPokemonIndex(null);

    };

    const openNextModal = () => {
        if (selectedPokemonIndex !== null && selectedPokemonIndex < queriedPokemonList.length - 1) {
            const nextPokemon = queriedPokemonList[selectedPokemonIndex + 1];
            setSelectedPokemon(nextPokemon);
            setSelectedPokemonIndex(selectedPokemonIndex + 1);
        }
    };

    const openPrevModal = () => {
        if (selectedPokemonIndex !== null && selectedPokemonIndex > 0) {
            const prevPokemon = queriedPokemonList[selectedPokemonIndex - 1];
            setSelectedPokemon(prevPokemon);
            setSelectedPokemonIndex(selectedPokemonIndex - 1);
        }
    };


    return (

            <Modal
                isOpen={true}
                onRequestClose={closeModal}
                className={'modal'}
            >
                <span className="closeBtn" onClick={closeModal}>&times;</span>
                <div className="display-card">
                    <img src={selectedPokemon!.img} alt={selectedPokemon!.name} />
                    <h1>{selectedPokemon!.name[0].toUpperCase() + selectedPokemon!.name.slice(1)}</h1>
                    <p>Index: {selectedPokemon!.idx}</p>
                    <p>Height: {selectedPokemon!.height}</p>
                    <p>Weight: {selectedPokemon!.weight}</p>
                    <div className="button-div">
                        <button className="prev-arrow" onClick={openPrevModal}>&#8592;</button>
                        <button className="next-arrow" onClick={openNextModal}>&#8594;</button>
                    </div>

                </div>


            </Modal>
    );
}

export default ModalView;
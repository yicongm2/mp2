import React from 'react';
import {useNavigate, useParams} from 'react-router-dom';
import "./main.css";

const PokemonDetails = (props: any) => {
    const { id } = useParams<{ id: string }>();
    const pokemonId = parseInt(id!, 10);
    const currentIndex = props.pokemonData.findIndex((p: { idx: number }) => p.idx === pokemonId);
    const navigate = useNavigate();

    if (!props.pokemonData[currentIndex]) {
        return <div>Loading...</div>;
    }

    const pokemon = props.pokemonData[currentIndex];

    const goToPreviousPokemon = () => {
        // console.log("total:" + props.pokemonData.length);
        // console.log("prev curr:" + currentIndex);
        const previousIndex = (currentIndex - 1 + props.pokemonData.length) % props.pokemonData.length;
        navigate(`/pokemon/${previousIndex}`)
    };

    const goToNextPokemon = () => {
        // console.log("next curr:" + currentIndex);
        const nextIndex = (currentIndex + 1) % props.pokemonData.length;
        navigate(`/pokemon/${nextIndex}`)
    };

    return (
        <div className="details-out-layer">
            <div className="button-div">
                <button className="prev-arrow" onClick={goToPreviousPokemon}>&#8592;</button>
            </div>
            <div className="details-card">
                <h1>{pokemon.name[0].toUpperCase() + pokemon.name.slice(1)}</h1>
                <img src={pokemon.img} alt={pokemon.name} />
                <p>Index: {pokemon.idx}</p>
                <p>Height: {pokemon.height}</p>
                <p>Weight: {pokemon.weight}</p>
                <p>Types: {pokemon.typeList.join(", ")}</p>
            </div>
            <div className="button-div">
                <button className="next-arrow" onClick={goToNextPokemon}>&#8594;</button>
            </div>
        </div>
    );
};

export default PokemonDetails;

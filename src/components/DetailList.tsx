import React from "react";
import { Link } from "react-router-dom";
import {Pokemon} from "./Gallery"

const PokemonList = (props: any) => {


    return (
        <div>
            <ul>
                {props.pokemonData.map((pokemon: Pokemon) => (
                    <li key={pokemon.idx}>
                        <Link to={`/pokemon/${pokemon.idx}`}>{pokemon.name}</Link>
                    </li>
                ))}
            </ul>
        </div>
    );
};

export default PokemonList;

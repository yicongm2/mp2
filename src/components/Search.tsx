import React, { useState, useEffect } from 'react';
import { Pokemon } from './Gallery';
import { Link } from 'react-router-dom';

const SearchView = (props: any) => {
    const [pokemonList, setPokemonList] = useState<Pokemon[]>([]);
    const [queriedPokemonList, setQueriedPokemonList] = useState<Pokemon[]>([]);
    const [searchTerm, setSearchTerm] = useState<string>('');
    const [sortBy, setSortBy] = useState<string>('name'); // 用于存储排序方式的状态
    const [sortOrder, setSortOrder] = useState<string>('ascending'); // 用于存储升序/降序状态的状态

    useEffect(() => {
        if (props.pokemonData) {
            setPokemonList(props.pokemonData);
        }
    }, [props.pokemonData]);

    const handleSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { value } = e.target;
        setSearchTerm(value);
        if (value === '') {
            setQueriedPokemonList([]);
        } else {
            const filteredPokemonList = pokemonList.filter((pokemon) =>
                pokemon.name.toLowerCase().includes(value.toLowerCase())
            );
            setQueriedPokemonList(filteredPokemonList);
        }
    };

    const handleSortByChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
        setSortBy(e.target.value);
    };

    const handleSortOrderChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
        setSortOrder(e.target.value);
    };

    const sortPokemonList = () => {
        let sortedPokemonList = [...queriedPokemonList];
        if (sortBy === 'name') {
            sortedPokemonList.sort((a, b) => {
                if (sortOrder === 'ascending') {
                    return a.name.localeCompare(b.name);
                } else {
                    return b.name.localeCompare(a.name);
                }
            });
        } else if (sortBy === 'idx') {
            sortedPokemonList.sort((a, b) => {
                if (sortOrder === 'ascending') {
                    return a.idx - b.idx;
                } else {
                    return b.idx - a.idx;
                }
            });
        }
        return sortedPokemonList;
    };

    return (
        <div className="searchResult">
            <input id="searchPokemon" onChange={handleSearch} value={searchTerm} placeholder="Search Pokemon" />
            <label htmlFor="sortBy">Sorted By:</label>
            <select id="sortBy" value={sortBy} onChange={handleSortByChange}>
                <option value="name">Name</option>
                <option value="idx">Index</option>
            </select>
            <label id="sortOrder" htmlFor="sortOrder">Ordered By:</label>
            <select value={sortOrder} onChange={handleSortOrderChange}>
                <option value="ascending">Ascending</option>
                <option value="descending">Descending</option>
            </select>

            <div className="pokemon-search">
                {sortPokemonList().map((pokemon: Pokemon, index: number) => (
                    <div className="pokemon-show" key={index}>
                        <Link to={`/pokemon/${pokemon.idx}`} className="pokemon-link">
                            <img src={pokemon.img} className="pokemon-img" alt={pokemon.name} />
                        </Link>
                        <p className="pokemon-name">{pokemon.idx + " " + pokemon.name[0].toUpperCase() + pokemon.name.slice(1)}</p>
                    </div>
                ))}
            </div>
        </div>
    );
};

export default SearchView;
